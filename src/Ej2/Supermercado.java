package Ej2;

import java.util.concurrent.Semaphore;

public class Supermercado {
    public static void main(String[] args) {
        Semaphore semaforo = new Semaphore(2);

        Caja caja1 = new Caja("1");
        Caja caja2 = new Caja("2");
        Caja caja3 = new Caja("3");

        CajaHilo hilo1 = new CajaHilo(caja1, "Andrew", 3, semaforo);
        CajaHilo hilo2 = new CajaHilo(caja2, "Mequita", 5, semaforo);
        CajaHilo hilo3 = new CajaHilo(caja3, "Valaitis", 2, semaforo);

        hilo1.start();
        hilo2.start();
        hilo3.start();
    }
}
