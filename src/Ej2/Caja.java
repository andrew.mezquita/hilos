package Ej2;

public class Caja {
    private String nombre;

    public Caja(String nombre) {
        this.nombre = nombre;
    }

    public void procesarCompra(String cliente, int productos) {
        try {
            System.out.println("La caja " + nombre + " recibe al cliente " + cliente);
            for (int i = 1; i <= productos; i++) {
                int executionTime = ((int) (Math.random() * 11)) * 1000;
                Thread.sleep(executionTime);
                System.out.println("Caja " + nombre + " - Cliente: " + cliente + " - Producto " + i);
            }
            System.out.println("La caja " + nombre + " vuelve a estar disponible");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
