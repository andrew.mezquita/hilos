package Ej2;

import java.util.concurrent.Semaphore;

public class CajaHilo extends Thread{
    private Caja caja;
    private String cliente;
    private int productos;
    private Semaphore semaforo;

    public CajaHilo(Caja caja, String cliente, int productos, Semaphore semaforo) {
        this.caja = caja;
        this.cliente = cliente;
        this.productos = productos;
        this.semaforo = semaforo;
    }

    @Override
    public void run() {
        try {
            semaforo.acquire();
            caja.procesarCompra(cliente, productos);
            semaforo.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
