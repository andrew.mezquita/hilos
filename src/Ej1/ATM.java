package Ej1;

import java.util.concurrent.Semaphore;

public class ATM implements Runnable{

    private int nombre;
    private Semaphore semaphore;

    public ATM(int name, Semaphore semaphore) {
        this.nombre = name;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        int executionTime = (int) (Math.random()*8)+3;
        try {
            System.out.println("ATM "+nombre+" está disponible");
            semaphore.acquire();

            for (int i = executionTime; i > 0; i--) {
                System.out.println("ATM "+nombre+" está en uso");
                Thread.sleep(1000);
            }
            System.out.println("ATM "+nombre+" vuelve a estar disponible");
            semaphore.release();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
