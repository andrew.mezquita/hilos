package Ej1;

import java.util.concurrent.Semaphore;

public class ATMMain {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);

        for (int i = 1; i <= 5; i++) {
            Thread hilo = new Thread(new ATM(i, semaphore));
            hilo.start();
        }
    }
}